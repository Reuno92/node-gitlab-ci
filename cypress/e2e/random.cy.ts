import * as cypress from 'cypress';

const baseUrl = process.env.BASE_URL || 'localhost:3000';

describe('Spec. for random page', () => {
  const occurences: Array<string> = ['titi', 'toto', 'tutu'];
  it('Visit Random page', () => {
    const randomPage =
      occurences[Math.round(Math.random() * (occurences.length - 1))];
    console.log();
    cy.visit(`http://${baseUrl}/${randomPage}`);
    cy.get('h1').contains('Resource not found');
    cy.get('img')
      .should('have.attr', 'src')
      .should(
        'include',
        'https://media.tenor.com/S9FVZ9P3GQwAAAAC/come-on-man-colin-jost.gif'
      );
  });
});
