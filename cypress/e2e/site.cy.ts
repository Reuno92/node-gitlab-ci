import * as cypress from 'cypress';

const baseUrl = process.env.BASE_URL || 'localhost:3000';

describe('Spec. for website page', () => {
  it('Visit website page', () => {
    cy.visit(`http://${baseUrl}/site`);
    cy.get('h1').contains('Hello World');
  });
});
