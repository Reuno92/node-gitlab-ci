import * as cypress from 'cypress';

const baseUrl = process.env.BASE_URL || 'localhost:3000';

describe('Spec. for first path', () => {
  it('Visit the first path', () => {
    cy.visit(`http://${baseUrl}`);
    cy.get('h1').contains('Server was started and online');
    cy.get('img')
      .should('have.attr', 'src')
      .should(
        'include',
        'https://media.tenor.com/ZI2UOhAouc0AAAAd/yes-saturday-night-live.gif'
      );
  });
});
