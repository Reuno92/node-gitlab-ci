import {Request, Response, Router} from 'express';

class Routes {
  public routes: Router = Router();

  constructor() {
    this.getHome();
  }

  private getHome(): void {
    this.routes.get('', (req: Request, res: Response) => {
      res.status(200).send('<h1>Hello World</h1>');
    });
  }
}

export default new Routes().routes;
