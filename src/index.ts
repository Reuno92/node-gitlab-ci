import express, {Request, Response} from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import webSiteRoutes from './routes/routes';

class Application {
  private app: express.Application = express();
  private readonly PORT: number = Number(process?.env?.PORT) || 3000;

  constructor() {
    this.globalConfiguration();
    this.routes();
    this.init();
  }

  private globalConfiguration() {
    this.app.use(
      cors({
        origin: '*',
      })
    );
    this.app.use(bodyParser.json());
    this.app.use(
      bodyParser.urlencoded({
        extended: false,
      })
    );
  }

  private routes() {
    this.app.use('/site', webSiteRoutes);
    this.app.get('', (req: Request, res: Response) => {
      res.status(200).send(
        `
        <section style="display: flex; flex-flow: column wrap; margin: 10vh auto 0; width: 60%; text-align: center">
            <h1>Server was started and online</h1>
            <img style="max-width: 50%; height: auto; margin: 0 auto" src="https://media.tenor.com/ZI2UOhAouc0AAAAd/yes-saturday-night-live.gif">
        </section>
        `
      );
    });

    this.app.all('*', (_, res: Response) => {
      res.status(200).send(`
       <section style="display: flex; flex-flow: column wrap; margin: 10vh auto 0; width: 60%; text-align: center">
           <h1>Resource not found</h1>
           <img style="max-width: 50%; height: auto; margin: 0 auto" src="https://media.tenor.com/S9FVZ9P3GQwAAAAC/come-on-man-colin-jost.gif" />
       </section>
      `);
    });
  }

  private init() {
    this.app.listen(this.PORT, () =>
      console.log('Server was started at http://localhost:%d', this.PORT)
    );
  }
}

new Application();
